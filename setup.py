import pathlib

from setuptools import setup


root_path = pathlib.Path(__file__).parent.resolve()
long_desc = (root_path / 'README.md').read_text(encoding='utf-8')

setup(
    name="basic_cowin_api",
    version="0.0.1",
    description="Python wrapper for Indian CoWin API"
                "https://apisetu.gov.in/public/marketplace/api/cowin/cowin-public-v2",
    long_description=long_desc,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/mayankjohri/cowin_api",
    author="Mayank Johri",
    author_email="mayankjohri@gmail.com",
    classifiers=[
        "Development Status :: 3 - Alpha",

        "Environment :: Console",

        "License :: OSI Approved :: MIT License",

        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.8",
        "Programming Language :: Python :: 3.9",
        "Programming Language :: Python :: 3 :: Only",
    ],
    keywords="cowin, covid, vaccine",
    packages=["basic_cowin_api"],
    include_package_data=True,
    python_requires=">=3.6",
    install_requires=[
        "pytest",
        "requests"
    ],
    project_urls={
        "Bug Reports": "https://gitlab.com/mayankjohri/cowin_api/-/issues",
        "Source": "https://gitlab.com/mayankjohri/cowin_api",
    },
)
